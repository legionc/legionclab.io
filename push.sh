#!/bin/sh
sudo find . -name ".DS_Store" -depth -exec rm {} \;
hexo clean && hexo g
git add -A
git commit -am "update"
git push origin --all
echo ---
echo All Done!
echo ---
